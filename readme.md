```
# get container id
docker ps
# run command inside docker
docker exec <container id> bx --info
```
# Contains
- bx
- bx container-service
- bx container-registry
- kubectl
