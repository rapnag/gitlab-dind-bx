FROM gitlab/dind

RUN curl -fsSL https://clis.ng.bluemix.net/install/linux | sh
RUN bx plugin install container-service -r Bluemix
RUN bx plugin install container-registry -r Bluemix
RUN bx plugin list
RUN curl -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v1.8.5/bin/linux/amd64/kubectl
RUN sudo chmod +x /usr/local/bin/kubectl
